import pandas as pd
import matplotlib.pyplot as plt
import os
import os.path
import numpy as np
import pyedflib
import warnings
import seaborn as sn

from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.decomposition import PCA
from sklearn.model_selection import KFold
from sklearn.metrics import f1_score, confusion_matrix
from evolutionary_search import EvolutionaryAlgorithmSearchCV
from sklearn.exceptions import ConvergenceWarning, UndefinedMetricWarning


def silence_warnings():
    """
    This feature silences unnecessary warnings.
    :return: filterwarnings
    """
    warnings.filterwarnings("ignore", category=FutureWarning)
    warnings.filterwarnings("ignore", category=ConvergenceWarning)
    warnings.filterwarnings("ignore", category=UndefinedMetricWarning)


def read_annotations(reader):
    """
    This feature reads details about events from .event file.
    :param reader: EdfReader object
    :return: list of numbers of indexes and list of event names
    """
    annotations = reader.readAnnotations()
    indexes = annotations[0] * 2048
    indexes = indexes.astype(int)
    events = preprocess_events(annotations[2])
    return indexes, events


def preprocess_events(events):
    """
    This feature replaces specified names of events to appropriates.
    :param events: list of event names
    :return: preprocessed list of event names
    """
    if events[1] != '#start':
        events[1] = '#start'
    for index, value in enumerate(events):
        if value == '56789#':
            events[index] = '56789_'
        elif value == 'FLRX4#':
            events[index] = 'FLRX4_'
        elif '#counted' in value:
            events[index] = '#counted'
        elif '#Tgt' in value:
            events[index] = '#Target'
    return events


def observations_reduction(df, ann):
    """
    This feature calculates a mean of non-event observations between two following events.
    :param df: DataFrame with full signals
    :param ann: list with indexes of events
    :return: reduced DataFrame
    """
    df_new = pd.DataFrame(columns=df.columns)
    for i in range(1, len(ann)):
        df_new = df_new.append(df.iloc[ann[i]], ignore_index=True)
        if i != len(ann)-1:
            df_new = df_new.append(df.iloc[range(ann[i] + 1, ann[i + 1])].mean(), ignore_index=True)
    return df_new


def read_signals_from_file(reader, header):
    """
    This feature reads signals from .edf files, pre-processes it and inserts to DataFrame.
    :param reader: EdfREeader object
    :param header: list of columns names
    :return: DataFrame
    """
    ann, events = read_annotations(reader)
    n = reader.signals_in_file
    data_list = np.zeros((n, reader.getNSamples()[0]))
    for i in np.arange(n):
        data_list[i, :] = reader.readSignal(i)
    data_list = np.transpose(data_list)
    df = pd.DataFrame(data=data_list[0:, 0:], columns=header)
    df.loc[ann, 'STI'] = events
    df['STI'] = df['STI'].fillna(0)
    df = observations_reduction(df, ann)
    return df


def edfs_to_pickle(binary=False, scale_type=''):
    """
    This function converts sets of .edf files to one .pkl file.
    :param binary: if events contain in 2 classes, default false
    :param scale_type: type of preprocessing - scale/standard. Default None
    :return: pkl file
    """
    if binary is True:
        name = 'dane-binary.pkl'
    else:
        name = 'dane.pkl'
    if scale_type != '':
        name = name[:-4] + str('-other') + scale_type + name[-4:]
    filenames = search_file_type('dane', '.edf')
    main_df = None
    header = []
    cnt = 0
    for f in filenames:
        edf = pyedflib.EdfReader(f)
        if not header:
            header_main = []
            header = edf.getSignalLabels()
            header_main.extend(header)
            header_main.append('STI')
            main_df = pd.DataFrame(columns=header_main)
        df = read_signals_from_file(edf, header)
        if scale_type != '':
            df = preprocessing_inside(df, scale_type)
        main_df = main_df.append(df, ignore_index=True)
        cnt += len(df.index)
        print(cnt, f)
    main_df['STI'] = categorical_to_numeric(main_df['STI'], binary)
    print(main_df)
    main_df.to_pickle(name)


def categorical_to_numeric(vector, binary):
    """
    This function converts categorical features to numeric.
    :param vector: categorical feature
    :param binary: if True events are one class, else events are in other classes
    :return: numeric vector
    """
    if binary is True:
        convert = {'#start': 1, '56789_': 1, 'ABCDEF': 1, 'AGMSY5': 1, 'BHNTZ6': 1, 'CIOU17': 1, 'DJPV28': 1,
                   'EKQW39': 1, 'FLRX4_': 1, 'GHIJKL': 1, 'MNOPQR': 1, 'STUVWX': 1, 'YZ1234': 1, '#counted': 1,
                   '#end': 1}
                   # '#Target': 1}
    else:
        convert = {'#start': 1, '56789_': 2, 'ABCDEF': 3, 'AGMSY5': 4, 'BHNTZ6': 5, 'CIOU17': 6, 'DJPV28': 7,
                   'EKQW39': 8, 'FLRX4_': 9, 'GHIJKL': 10, 'MNOPQR': 11, 'STUVWX': 12, 'YZ1234': 13, '#counted': 14,
                   '#end': 15}
                   # '#Target': 16}
    vector.replace(convert, inplace=True)
    return vector.astype(int)


def open_df_from_pickle(name):
    """
    This function opens specific .pkl file and loads it to DataFrame.
    :param name: path to .pkl file without extension
    :return: DataFrame
    """
    df = pd.read_pickle(name + str('.pkl'))
    return df


def dimension_reduction(source='dane'):
    """
    This function uses PCA to reduce dimension of DataFrame.
    :param source: name of .pkl file, default 'dane'
    :return: .pkl file with reduced data
    """
    source += str('.pkl')
    df = pd.read_pickle(source)
    pca = PCA(n_components=.8)
    y = df['STI']
    del df['STI']
    new_df = pca.fit_transform(df)
    df = pd.DataFrame(new_df)
    df['STI'] = y
    df.to_pickle(source[:-4] + str('_PCA') + source[-4:])


def search_file_type(path, file_type):
    """
    This function returns all file paths of a specified type from input directory.
    :param path: path with files
    :param file_type: file extension
    :return: list of file paths
    """
    files = []
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in [f for f in filenames if f.endswith(file_type)]:
            files.append(os.path.join(dirpath, filename))
    return files


def scale_data(df, scale_type):
    """
    This feature scales or standardizes data.
    :param df: DataFrame
    :param scale_type: type of preprocessing: scale or standard, default scale if other
    :return: scaled DataFrame
    """
    columns = df.columns
    if scale_type == 'standard':
        scaler = StandardScaler()
    else:
        scaler = MinMaxScaler(feature_range=(-1, 1))
    df = scaler.fit_transform(df)
    return pd.DataFrame(df, columns=columns)


def preprocessing_inside(df, scale_type):
    """
    This function preprocesses one patient's file.
    :param df: DataFrame with patient's data
    :param scale_type: type of preprocessing: scale or standard, default scale if other
    :return: DataFrame with preprocessed data
    """
    if scale_type not in ['scale', 'standard']:
        scale_type = 'scale'
    y = df['STI']
    del df['STI']
    df = scale_data(df, scale_type)
    df['STI'] = y
    return df


def preprocessing(source='dane', scale_type='scale'):
    """
    This function opens a specified .pkl file, preprocesses data and saves these to new .pkl file.
    :param source: path to .pkl file, default 'dane'
    :param scale_type: type of preprocessing: scale or standard, default scale
    :return: .pkl file with preprocessed data
    """
    if scale_type not in ['scale', 'standard']:
        scale_type = 'scale'
    df = open_df_from_pickle(source)
    y = df['STI']
    del df['STI']
    df = scale_data(df, scale_type)
    df['STI'] = y
    print(df)
    df.to_pickle(source + str('-') + scale_type + str('.pkl'))


def learning(source, classifier_name, params):
    """
    This feature learns data with best parameters of classifiers and print accuracy and F1-score.
    :param source: path to .pkl file
    :param classifier_name: name of using classifier
    :param params: parameters of classifier
    :return: F1-score, accuracy of test and train
    """
    classifier_dict = {'SVC': SVC(), 'kNN': KNeighborsClassifier(), 'Decision Tree': DecisionTreeClassifier(),
                       'Random Forest': RandomForestClassifier(), 'Gaussian NB': GaussianNB(),
                       'AdaBoost': AdaBoostClassifier(), 'QDA': QuadraticDiscriminantAnalysis()}
    df = open_df_from_pickle(source)
    y = df['STI'].values
    del df['STI']
    df = df.values
    clf = classifier_dict[classifier_name]
    clf.set_params(**params)
    kf = KFold(n_splits=10)
    f1score = np.array([])
    accuracy = np.array([])
    train_score = np.array([])
    if 'binary' in source:
        cm = np.zeros((2, 2))
        labels = [0, 1]
        vmax = None
    else:
        cm = np.zeros((16, 16))
        labels = np.arange(0, 16, 1)
        vmax = 2600
    for train, test in kf.split(df):
        x_train, x_test = df[train], df[test]
        y_train, y_test = y[train], y[test]
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)
        f1score = np.append(f1score, f1_score(y_test, y_pred, average='macro'))
        accuracy = np.append(accuracy, clf.score(x_test, y_test))
        if classifier_name == 'kNN':
            train_score = np.array([1.0])
        else:
            train_score = np.append(train_score, clf.score(x_train, y_train))
        cm += confusion_matrix(y_test, y_pred, labels=labels)
    print(classifier_name, '{0:.4f}'.format(f1score.mean()), '{0:.4f}'.format(accuracy.mean()),
          '{0:.4f}'.format(train_score.mean()))
    plot_confusion_matrix(cm, labels, classifier_name, source, vmax)


def learning_evolutionary(clf, paramgrid, source):
    """
    This feature finds the best parameters of using classifier.
    :param clf: name of classifier
    :param paramgrid: dictionary with names of parameters and their ranges
    :param source: path to .pkl file
    :return: dict of the best values of parameters
    """
    df = open_df_from_pickle(source)[:10000]
    y = df['STI'].values
    del df['STI']
    df = df.values

    cv = KFold(n_splits=10)
    search = EvolutionaryAlgorithmSearchCV(estimator=clf,
                                           params=paramgrid,
                                           cv=cv,
                                           population_size=50,
                                           gene_mutation_prob=0.2,
                                           gene_crossover_prob=0.5,
                                           tournament_size=2,
                                           generations_number=10)
    search.fit(df, y)
    results = pd.DataFrame(search.cv_results_).sort_values("mean_test_score", ascending=False)
    print(results[['mean_test_score', 'min_test_score', 'max_test_score']])
    print(results['params'][0])


def count_class():
    """
    This function counts the number of classes in the data.
    :return: printed values
    """
    df = open_df_from_pickle('dane-binary-otherscale_PCA-scale')
    print(df)
    print(df['STI'].value_counts())


def plot_signals(is_reduction=False, is_pca=False):
    """
    This function plots specified signal.
    :param is_reduction: flag is signals are after feature reductions, default false
    :param is_pca: flag is signals are after PCA, default false
    :return: signal plots
    """
    filename = 'dane/s03/rc05.edf'
    edf = pyedflib.EdfReader(filename)
    header = edf.getSignalLabels()
    if is_reduction:
        df = read_signals_from_file(edf, header)
    else:
        n = edf.signals_in_file
        data_list = np.zeros((n, edf.getNSamples()[0]))
        for i in np.arange(n):
            data_list[i, :] = edf.readSignal(i)
        data_list = np.transpose(data_list)
        df = pd.DataFrame(data=data_list[0:, 0:], columns=header)
    if is_pca:
        pca = PCA(n_components=.8)
        del df['STI']
        new_df = pca.fit_transform(df)
        df = pd.DataFrame(new_df)
    ax = df[df.columns[0:5]].plot(subplots='True', figsize=(20, 40), grid=True, sharey=True)
    ax[2].set_ylabel('potencjał [uV]', labelpad=10)
    plt.xlabel('numer obserwacji')
    plt.show()


def get_class_names(vector):
    """
    This function get class names.
    :param vector: list of values
    :return: list of names
    """
    if len(vector) == 2:
        return ['brak bodźca', 'bodziec']
    else:
        convert = {'brak': 0, '#start': 1, '56789_': 2, 'ABCDEF': 3, 'AGMSY5': 4, 'BHNTZ6': 5, 'CIOU17': 6, 'DJPV28': 7,
                   'EKQW39': 8, 'FLRX4_': 9, 'GHIJKL': 10, 'MNOPQR': 11, 'STUVWX': 12, 'YZ1234': 13, '#counted': 14,
                   '#end': 15}
        return convert


def plot_confusion_matrix(cm, labels, title, file_name, vmax=None):
    """
    This function plot confusion matrices.
    :param cm: NxN array confusion matrix
    :param labels: list of indexes of class names
    :param title: title of the plot
    :param file_name: path to save the file
    :param vmax: max range of heatmap
    :return: plot confusion matrix
    """
    columns = get_class_names(labels)
    df_cm = pd.DataFrame(cm, index=columns, columns=columns, dtype=int)
    imagepath = 'macierze/' + file_name
    plt.figure(figsize=(14, 10))
    sn.heatmap(df_cm, annot=True, annot_kws={"size": 12}, fmt="d", cmap="YlGnBu", vmin=0, vmax=vmax)
    plt.subplots_adjust(left=0.1, bottom=0.14, right=1, top=0.92, wspace=0, hspace=0)
    plt.ylabel('Wartość oczekiwana', size=14, labelpad=20)
    plt.xlabel('Wartość przewidywana', size=14, labelpad=20)
    plt.title('Macierz błędów - ' + title, size=20, pad=20)
    if not os.path.isdir(imagepath):
        os.makedirs(imagepath)
    plt.savefig(imagepath + '/' + title + '.png')
    plt.close()


# ==========================
#           MAIN
# ==========================
silence_warnings()
# Change all .edf files to one .pkl
# edfs_to_pickle()
# edfs_to_pickle(binary=True)
# edfs_to_pickle(scale_type='scale')
# edfs_to_pickle(binary=True, scale_type='scale')
# edfs_to_pickle(scale_type='standard')
# edfs_to_pickle(binary=True, scale_type='standard')
#
# Dimension reduction to 5 features
# dimension_reduction('dane')
# dimension_reduction('dane-binary')
# dimension_reduction('dane-otherscale')
# dimension_reduction('dane-binary-otherscale')
# dimension_reduction('dane-otherstandard')
# dimension_reduction('dane-binary-otherstandard')
#
# Preprocessing
# preprocessing('dane', 'scale')
# preprocessing('dane', 'standard')
# preprocessing('dane_PCA', 'scale')
# preprocessing('dane_PCA', 'standard')
# preprocessing('dane-binary', 'scale')
# preprocessing('dane-binary', 'standard')
# preprocessing('dane-binary_PCA', 'scale')
# preprocessing('dane-binary_PCA', 'standard')
# preprocessing('dane-binary-otherscale_PCA', 'scale')
# preprocessing('dane-binary-otherscale_PCA', 'standard')
# preprocessing('dane-binary-otherstandard_PCA', 'scale')
# preprocessing('dane-binary-otherstandard_PCA', 'standard')

# count_class()

# Parameter optimization - genetic algorithm
# filename = 'dane_PCA-standard'

# --------------------SVC--------------------
# clf = SVC()
# cv = KFold(n_splits=10)
# paramgrid = {"kernel": ["sigmoid"],
#              "C": np.logspace(-12, 12, num=1000, base=10),
#              "gamma": np.logspace(-9, 9, num=1000, base=10),
#              "max_iter": [100]}
# learning_evolutionary(clf, paramgrid, filename)
# paramgrid = {"kernel": ["rbf"],
#              "C": np.logspace(-12, 12, num=1000, base=10),
#              "gamma": np.logspace(-9, 9, num=1000, base=10),
#              "max_iter": [100]}
# learning_evolutionary(clf, paramgrid, filename)
# paramgrid = {"kernel": ["linear"],
#              "C": np.logspace(-12, 12, num=1000, base=10),
#              "gamma": np.logspace(-9, 9, num=1000, base=10),
#              "max_iter": [100]}
# learning_evolutionary(clf, paramgrid, filename)
# paramgrid = {"kernel": ["poly"],
#              "C": np.logspace(-12, 12, num=1000, base=10),
#              "gamma": np.logspace(-9, 9, num=1000, base=10),
#              "max_iter": [100]}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------kNN--------------------
# clf = KNeighborsClassifier()
# cv = KFold(n_splits=10)
# paramgrid = {"n_neighbors": np.arange(1, 2000, 1),
#              "weights": ["uniform", "distance"],
#              "algorithm": ["ball_tree", "kd_tree"],
#              "p": [1, 2]}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------Decision Tree--------------------
# clf = DecisionTreeClassifier()
# cv = KFold(n_splits=10)
# paramgrid = {"criterion": ["gini", "entropy"],
#              "splitter": ["best", "random"],
#              "min_samples_split": np.arange(2, 100, 1),
#              "min_samples_leaf": np.arange(1, 100, 1),
#              "min_weight_fraction_leaf": np.arange(0, 0.5, 0.01)}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------Random Forest--------------------
# clf = RandomForestClassifier()
# cv = KFold(n_splits=10)
# paramgrid = {"n_estimators": np.arange(1, 100, 1),
#              "criterion": ["gini", "entropy"],
#              "min_samples_split": np.arange(2, 100, 1),
#              "min_samples_leaf": np.arange(1, 100, 1),
#              "min_weight_fraction_leaf": np.arange(0, 0.5, 0.01),
#              "n_jobs": [2],
#              "verbose": np.arange(0, 100, 1),
#              "class_weight": ["balanced", "balanced_subsample", None]}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------Gaussian Process--------------------
# clf = GaussianProcessClassifier()
# cv = KFold(n_splits=10)
# paramgrid = {"n_restarts_optimizer": np.arange(0, 100, 1),
#              "max_iter_predict": np.arange(100, 10000, 100),
#              "multi_class": ["one_vs_rest", "one_vs_one"],
#              "n_jobs": [-1]}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------Gaussian NB--------------------
# clf = GaussianNB()
# cv = KFold(n_splits=10)
# paramgrid = {"var_smoothing": np.logspace(-12, 0, num=13, base=10)}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------AdaBoost--------------------
# clf = AdaBoostClassifier()
# cv = KFold(n_splits=10)
# paramgrid = {"n_estimators": np.arange(1, 1000, 1),
#              "learning_rate": np.logspace(0, 1, num=100, base=10),
#              "algorithm": ["SAMME", "SAMME.R"]}
# learning_evolutionary(clf, paramgrid, filename)

# --------------------QDA--------------------
# clf = QuadraticDiscriminantAnalysis()
# cv = KFold(n_splits=10)
# paramgrid = {"priors": np.arange(1, 200, 1),
#              "tol": np.logspace(-12, 0, num=13, base=10)}
# learning_evolutionary(clf, paramgrid, filename)

# print(filename)

# Learning
# --------------------dane_PCA--------------------
# filename = 'dane_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 4.331483223376394, 'gamma': 19421746.814890284, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 0.9203731996618231, 'gamma': 17.14881969870541, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 4391800.892596077, 'gamma': 2.821307675939471e-09, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 107902.87915161814, 'gamma': 4.271993966306776e-05, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 316, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'best', 'min_samples_split': 31, 'min_samples_leaf': 75,
#           'min_weight_fraction_leaf': 0.38}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 65, 'criterion': 'gini', 'min_samples_split': 29, 'min_samples_leaf': 44,
#           'min_weight_fraction_leaf': 0.35000000000000003, 'n_jobs': 2, 'verbose': 61, 'class_weight': 'balanced'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 0.01}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 262, 'learning_rate': 1.788649529057435, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane_PCA-scale--------------------
# filename = 'dane_PCA-scale'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 3959110266.4684668, 'gamma': 22927693.128656488, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 1.9421746814890286e-12, 'gamma': 559.4325706169366, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 0.00017387624002162505, 'gamma': 23408.272761782944, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 0.03330600343624582, 'gamma': 84708.68266557419, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 345, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'random', 'min_samples_split': 79, 'min_samples_leaf': 15,
#           'min_weight_fraction_leaf': 0.33}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 19, 'criterion': 'entropy', 'min_samples_split': 5, 'min_samples_leaf': 51,
#           'min_weight_fraction_leaf': 0.24, 'n_jobs': 2, 'verbose': 36, 'class_weight': None}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 0.1}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 531, 'learning_rate': 2.718588242732941, 'algorithm': 'SAMME'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane_PCA-standard--------------------
# filename = 'dane_PCA-standard'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 150375.53212997416, 'gamma': 0.0014526539259467812, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 0.046415888336127725, 'gamma': 51488.67450137487, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 4.04209583979631e-06, 'gamma': 1393.6192742241435, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 1.8891927762076642e-08, 'gamma': 0.0008829699955494083, 'max_iter': 100}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 446, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'best', 'min_samples_split': 16, 'min_samples_leaf': 27,
#           'min_weight_fraction_leaf': 0.4}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 48, 'criterion': 'gini', 'min_samples_split': 91, 'min_samples_leaf': 20,
#           'min_weight_fraction_leaf': 0.18, 'n_jobs': 2, 'verbose': 93, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 0.01}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 778, 'learning_rate': 2.9836472402833394, 'algorithm': 'SAMME'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary_PCA--------------------
# filename = 'dane-binary_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 2154434690.031878, 'gamma': 18.6324631193156, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 9.077326525210223e-07, 'gamma': 15141893.25304352, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 0.591250841383187, 'gamma': 161.14142772530167, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 0.00031952475057592136, 'gamma': 8.648423275731709e-07, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 1134, 'weights': 'uniform', 'algorithm': 'ball_tree', 'p': 1}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'random', 'min_samples_split': 41,
#           'min_samples_leaf': 24, 'min_weight_fraction_leaf': 0.13}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 6, 'criterion': 'entropy', 'min_samples_split': 25,
#           'min_samples_leaf': 10, 'min_weight_fraction_leaf': 0.21, 'n_jobs': 2, 'verbose': 36,
#           'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-09}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 670, 'learning_rate': 8.902150854450388, 'algorithm': 'SAMME'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary_PCA-scale--------------------
# filename = 'dane-binary_PCA-scale'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 53366992.31206313, 'gamma': 53.36699231206324, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 1232.8467394420659, 'gamma': 18738.174228603868, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 14563484.775012445, 'gamma': 33932217.7189533, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 1.8738174228603839, 'gamma': 0.0006579332246575682, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 132, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 1}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'best', 'min_samples_split': 19, 'min_samples_leaf': 27,
#           'min_weight_fraction_leaf': 0.36}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 47, 'criterion': 'gini', 'min_samples_split': 18, 'min_samples_leaf': 8,
#           'min_weight_fraction_leaf': 0.14, 'n_jobs': 2, 'verbose': 77, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-08}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 917, 'learning_rate': 1.4174741629268053, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary_PCA-standard--------------------
# filename = 'dane-binary_PCA-standard'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 30653952950.56536, 'gamma': 4.1842885079015845e-08, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 16681005372.000557, 'gamma': 136500780.65460083, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 0.020244465099768016, 'gamma': 1.6114142772530167, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 477.1760948938751, 'gamma': 243.99862972595452, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 1169, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 1}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'best', 'min_samples_split': 89, 'min_samples_leaf': 82,
#           'min_weight_fraction_leaf': 0.22}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 86, 'criterion': 'entropy', 'min_samples_split': 94, 'min_samples_leaf': 23,
#           'min_weight_fraction_leaf': 0.03, 'n_jobs': 2, 'verbose': 31, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-06}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 619, 'learning_rate': 4.7508101621027965, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-otherscale_PCA--------------------
# filename = 'dane-otherscale_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 0.05184593543892913, 'gamma': 30024.61709085543, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 5.594325706169389e-09, 'gamma': 0.23898925662310527, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 4609.604486828439, 'gamma': 54.794723369002924, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 4422273980.505915, 'gamma': 0.06604193962330306, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 97, 'criterion': 'gini', 'min_samples_split': 74, 'min_samples_leaf': 15,
#           'min_weight_fraction_leaf': 0.36, 'n_jobs': 2, 'verbose': 66, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-09}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 970, 'learning_rate': 1.7475284000076838, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-otherstandard_PCA--------------------
# filename = 'dane-otherstandard_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 1.2915496650148827e-05, 'gamma': 0.7636298261282257, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 0.0036438589837635405, 'gamma': 0.3330600343624582, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 6.9317172761554e-12, 'gamma': 0.06335804992658255, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 2.782559402207126, 'gamma': 4.939621743878326e-06, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 676, 'weights': 'distance', 'algorithm': 'kd_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'best', 'min_samples_split': 98, 'min_samples_leaf': 64,
#           'min_weight_fraction_leaf': 0.28}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 38, 'criterion': 'gini', 'min_samples_split': 79, 'min_samples_leaf': 52,
#           'min_weight_fraction_leaf': 0.02, 'n_jobs': 2, 'verbose': 8, 'class_weight': None}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 0.01}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 387, 'learning_rate': 5.3366992312063095, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherscale_PCA--------------------
# filename = 'dane-binary-otherscale_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 2.0244465099768018e-10, 'gamma': 3.618749812411277e-05, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 1035.1779556301763, 'gamma': 1.1092898648952228e-06, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 3520031.4727966897, 'gamma': 7.175560918936921, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 0.0393182875570577, 'gamma': 3.4004119327037025e-08, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 36, 'weights': 'uniform', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'best', 'min_samples_split': 29, 'min_samples_leaf': 32,
#           'min_weight_fraction_leaf': 0.32}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 15, 'criterion': 'entropy', 'min_samples_split': 64, 'min_samples_leaf': 72,
#           'min_weight_fraction_leaf': 0.02, 'n_jobs': 2, 'verbose': 80, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-10}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 802, 'learning_rate': 2.25701971963392, 'algorithm': 'SAMME'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherscale_PCA-scale--------------------
# filename = 'dane-binary-otherscale_PCA-scale'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 451.4967772036102, 'gamma': 1042.3606739764032, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 144264395121.8159, 'gamma': 6.468607661546321e-09, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 3.8778284145894615, 'gamma': 2.6510836019085415e-06, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 1.38401609657313e-07, 'gamma': 10.865157746525373, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 1802, 'weights': 'uniform', 'algorithm': 'ball_tree', 'p': 1}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'best', 'min_samples_split': 50, 'min_samples_leaf': 62,
#           'min_weight_fraction_leaf': 0.17}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 29, 'criterion': 'entropy', 'min_samples_split': 88, 'min_samples_leaf': 98,
#           'min_weight_fraction_leaf': 0.04, 'n_jobs': 2, 'verbose': 0, 'class_weight': 'balanced'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-08}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 582, 'learning_rate': 1.788649529057435, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherscale_PCA-standard--------------------
# filename = 'dane-binary-otherscale_PCA-standard'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 3002461709.0855556, 'gamma': 0.0033306003436245884, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 2.066880249629082e-09, 'gamma': 1336.9837418249451, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 882969.9955494119, 'gamma': 1578.3314056521197, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 0.0007742636826811277, 'gamma': 37.720424934169934, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 223, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'gini', 'splitter': 'best', 'min_samples_split': 10, 'min_samples_leaf': 88,
#           'min_weight_fraction_leaf': 0.08}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 50, 'criterion': 'entropy', 'min_samples_split': 51, 'min_samples_leaf': 54,
#           'min_weight_fraction_leaf': 0.23, 'n_jobs': 2, 'verbose': 17, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 0.0001}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 88, 'learning_rate': 1.2618568830660204, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherstandard_PCA--------------------
# filename = 'dane-binary-otherstandard_PCA'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 2373.424250023866, 'gamma': 22.927693128656486, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 81830.06815867373, 'gamma': 0.18632463119315598, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 28804441.53396298, 'gamma': 4.014242490499326, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 504.31594871713486, 'gamma': 1.483102514336103e-08, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 187, 'weights': 'uniform', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'random', 'min_samples_split': 37, 'min_samples_leaf': 30,
#           'min_weight_fraction_leaf': 0.08}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 48, 'criterion': 'entropy', 'min_samples_split': 32, 'min_samples_leaf': 56,
#           'min_weight_fraction_leaf': 0.11, 'n_jobs': 2, 'verbose': 79, 'class_weight': None}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-06}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 535, 'learning_rate': 2.420128264794382, 'algorithm': 'SAMME'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherstandard_PCA-scale--------------------
# filename = 'dane-binary-otherstandard_PCA-scale'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 0.0032622220097116733, 'gamma': 3.5444556739704356, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 6789.406812696098, 'gamma': 7.959777002314977e-09, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 8952.657125996382, 'gamma': 67426222.41778348, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 114831241.45435111, 'gamma': 0.14526539259467783, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 897, 'weights': 'distance', 'algorithm': 'ball_tree', 'p': 2}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'best', 'min_samples_split': 65, 'min_samples_leaf': 70,
#           'min_weight_fraction_leaf': 0.05}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 48, 'criterion': 'gini', 'min_samples_split': 17, 'min_samples_leaf': 80,
#           'min_weight_fraction_leaf': 0.23, 'n_jobs': 2, 'verbose': 57, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-07}
# learning(filename, classifier_name, params)
# classifier_name = 'AdaBoost'
# params = {'n_estimators': 609, 'learning_rate': 2.420128264794382, 'algorithm': 'SAMME.R'}
# learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

# --------------------dane-binary-otherstandard_PCA-standard--------------------
filename = 'dane-binary-otherstandard_PCA-standard'
# classifier_name = 'SVC'
# params = {'kernel': 'sigmoid', 'C': 5.479472336900293e-10, 'gamma': 53669.76945540476, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'rbf', 'C': 540421642.0705904, 'gamma': 2.5966559729348724e-09, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'linear', 'C': 0.25787628875937985, 'gamma': 234082727.61782944, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'SVC'
# params = {'kernel': 'poly', 'C': 2.508415059277541e-05, 'gamma': 6335804.992658242, 'max_iter': 1000}
# learning(filename, classifier_name, params)
# classifier_name = 'kNN'
# params = {'n_neighbors': 1248, 'weights': 'uniform', 'algorithm': 'kd_tree', 'p': 1}
# learning(filename, classifier_name, params)
# classifier_name = 'Decision Tree'
# params = {'criterion': 'entropy', 'splitter': 'random', 'min_samples_split': 67, 'min_samples_leaf': 28,
#           'min_weight_fraction_leaf': 0.22}
# learning(filename, classifier_name, params)
# classifier_name = 'Random Forest'
# params = {'n_estimators': 24, 'criterion': 'gini', 'min_samples_split': 40, 'min_samples_leaf': 14,
#           'min_weight_fraction_leaf': 0.01, 'n_jobs': 2, 'verbose': 33, 'class_weight': 'balanced_subsample'}
# learning(filename, classifier_name, params)
# classifier_name = 'Gaussian NB'
# params = {'var_smoothing': 1e-05}
# learning(filename, classifier_name, params)
classifier_name = 'AdaBoost'
params = {'n_estimators': 645, 'learning_rate': 1.1768119524349985, 'algorithm': 'SAMME'}
learning(filename, classifier_name, params)
# classifier_name = 'QDA'
# params = {}
# learning(filename, classifier_name, params)

print(filename)
# plot_signals()

